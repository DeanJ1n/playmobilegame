﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // PRIVATE FIELDS
    private int point = 0;

    // Make this class using the single ton pattern
    public static GameManager instance = null;

    // Create the singleton in awake
    private void Awake()
    {
        if (!instance)
        {
            instance = this;
        }
        else if(instance != this)
        {
            Destroy(gameObject);
        }

        // Make the game object last through levels
        DontDestroyOnLoad(gameObject);
    }

    // Getter adn setter for points
    public int GetPoint() { return point; }
    public void SetPoint(int pointToSet) { point = pointToSet; }
}
