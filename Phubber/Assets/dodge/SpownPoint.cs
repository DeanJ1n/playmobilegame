﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpownPoint : MonoBehaviour {

    public GameObject passByPrefab;

	// Use this for initialization
	void Start () {

        Instantiate(passByPrefab, transform.position, Quaternion.identity);

    }
	
	// Update is called once per frame
	void Update () {
        //Destroy(gameObject);
    }

    
}
