﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpownTreePoint : MonoBehaviour
{

    public GameObject treePrefab;

    public float treetime = 0f;

    // Use this for initialization
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        if (treetime <= 0)
        {
            Instantiate(treePrefab, transform.position, Quaternion.identity);
            treetime = Random.Range(0f, 4f);
        }
        else
        {
            treetime -= Time.deltaTime;
        }
    }


}

