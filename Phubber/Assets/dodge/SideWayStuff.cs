﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideWayStuff : MonoBehaviour
{

    public float speed = 5;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        transform.Translate(Vector2.left * speed * Time.deltaTime);

    }

    private void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
        Debug.Log("tree triggered");
    }
} 

