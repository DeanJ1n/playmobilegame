﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    // SERIALIZED FIELDS
    [SerializeField] Text pointText;
    [SerializeField] Image GameOverScene;
    [SerializeField] GameObject restartButton;
    [SerializeField] Text endGameText;

    private GameManager manager;
    private Vector3 targetPos;
    private int point;
    private bool playing = true;
    public float Zincrement;

    public float speed;
    public float leftBound;
    public float rightBound;

    // Use a start to start counting points
    void Start()
    {
        manager = FindObjectOfType<GameManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        GameEnd();
    }

    public void Restart()
    {
        manager.SetPoint(0);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void GameEnd()
    {
        manager.SetPoint(point);
        playing = false;
        restartButton.SetActive(true); 
        GameOverScene.GetComponent<Animator>().enabled = true;
        endGameText.GetComponent<Animator>().enabled = true;
    }

    public int GetPlayerPoint() { return point; }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);

        if (Input.GetKeyDown(KeyCode.D) && transform.position.z > leftBound)
        {
            targetPos = new Vector3(transform.position.x, transform.position.y, transform.position.z - Zincrement);
        }
        else if (Input.GetKeyDown(KeyCode.A) && transform.position.z < rightBound)
        {
            targetPos = new Vector3(transform.position.x, transform.position.y, transform.position.z + Zincrement);
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        // set the point to time elapsed
        if (playing)
        {
            point = (int)Time.timeSinceLevelLoad;
            pointText.text = "Time Score: " + point.ToString();
        }
    }
}
