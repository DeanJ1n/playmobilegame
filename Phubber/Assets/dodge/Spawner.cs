﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    private float timeToSpawn;
    public float gapTime = 1000f;
    public float decreaceGap = 1f;
    public float minTime = 100f;

    public GameObject[] passByPrefabs;

    private void Start()
    {
        timeToSpawn = gapTime;
    }

    private void Update()
    {
        if (timeToSpawn <= 0)
        {
            int randomNum = Random.Range(0, passByPrefabs.Length);
            Instantiate(passByPrefabs[randomNum], transform.position, Quaternion.identity);
            timeToSpawn = gapTime;
            if (gapTime > minTime)
            {
                gapTime -= decreaceGap;
            }
        }
        else
        {
            timeToSpawn -= Time.deltaTime;
        }

    
    }
}
