﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceHolder : MonoBehaviour {

    // Here is a enum representing current space status
    public enum SpaceState { Head, Body, Food, Empty}

    // SERIALIZED FIELDS
    // Here are the cached object of snake's head
    [SerializeField] GameObject head;
    [SerializeField] GameObject block;
    // Here are the materials to switch
    [SerializeField] Material headMaterial;
    [SerializeField] Material bodyMaterial;
    [SerializeField] Material foodMaterial;

    //PRIVATE FIELDS
    private SpaceState status = SpaceState.Empty;
    private SpaceHolder up = null;
    private SpaceHolder down = null;
    private SpaceHolder left = null;
    private SpaceHolder right = null;

    // Use this for initialization
    void Start () {
        head.GetComponent<MeshRenderer>().material = foodMaterial;
	}
	
    // Methods for linking spaces
    public void SetUp(SpaceHolder upSpace) { up = upSpace; }
    public void SetDown(SpaceHolder downSpace) { down = downSpace; }
    public void SetLeft(SpaceHolder leftSpace) { left = leftSpace; }
    public void SetRight(SpaceHolder rightSpace) { right = rightSpace; }

    // Methods for getting space
    public SpaceHolder GetUp() { return up; }
    public SpaceHolder GetDown() { return down; }
    public SpaceHolder GetLeft() { return left; }
    public SpaceHolder GetRight() { return right; }

    // Here is the function to set the state of current object
    public void SetStatus(SpaceState stateToSet)
    {
        // Set the status of the current space
        status = stateToSet;

        // Change the looking of the sapce
        if(status == SpaceState.Empty)
        {
            head.GetComponent<MeshRenderer>().enabled = false;
            block.GetComponent<MeshRenderer>().enabled = false;
        }else if(status == SpaceState.Body)
        {
            head.GetComponent<MeshRenderer>().enabled = false;
            block.GetComponent<MeshRenderer>().material = bodyMaterial;
            block.GetComponent<MeshRenderer>().enabled = true;
        }else if(status == SpaceState.Head)
        {
            head.GetComponent<MeshRenderer>().material = headMaterial;
            head.GetComponent<MeshRenderer>().enabled = true;
            block.GetComponent<MeshRenderer>().enabled = false;
        }
        else
        {
            head.GetComponent<MeshRenderer>().enabled = false;
            block.GetComponent<MeshRenderer>().material = foodMaterial;
            block.GetComponent<MeshRenderer>().enabled = true;
        }
    }

    // Here is the getter of the status
    public SpaceState GetStatus()
    {
        return status;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
