﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snake : MonoBehaviour {

    // SERIALIZED FIELD
    [SerializeField] float moveDelay = 0.3f;

    // PRIVATE FIELDS
    private char inputDir = 'u';
    private char dir = 'u'; //direction of the snake
    private SpaceHolder head;
    private List<SpaceHolder> body = new List<SpaceHolder>();
    private SpaceHolder food;
    private GameSpace gameSpace;


	// Use this for initialization
	void Start () {

	}
	
    public void SpawnSnake(SpaceHolder snakeHead, SpaceHolder body_1, SpaceHolder body_2)
    {
        // Cache game space
        gameSpace = FindObjectOfType<GameSpace>();
        head = snakeHead;
        body.Add(body_1);
        body.Add(body_2); 
        InvokeRepeating("Move", moveDelay, moveDelay);  //move the snake per 300 ms
        gameSpace.SpawnFood();
    }

	// Update is called once per frame
	void Update ()
    {
        //update the direction of the snake if a direction key is pushed
        if (Input.GetKey(KeyCode.RightArrow))
        {
            if(dir != 'l')
            {
                inputDir = 'r';
            }
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            if (dir != 'r')
            {
                inputDir = 'l';
            }
        }
        else if (Input.GetKey(KeyCode.UpArrow))
        {
            if (dir != 'd')
            {
                inputDir = 'u';
            }
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            if (dir != 'u')
            {
                inputDir = 'd';
            }
        }
    }

    void Move()
    {
        dir = inputDir;
        SpaceHolder.SpaceState tempState;
        if(dir == 'r')
        {
            tempState = head.GetRight().GetStatus();
            if (tempState == SpaceHolder.SpaceState.Empty)
            {
                // reshape the body if nothing on the corresponding place
                body.Add(head);
                head.SetStatus(SpaceHolder.SpaceState.Body);
                head = head.GetRight();
                head.SetStatus(SpaceHolder.SpaceState.Head);
                body[0].SetStatus(SpaceHolder.SpaceState.Empty);
                body.RemoveAt(0);
            }else if(tempState == SpaceHolder.SpaceState.Food)
            {
                // adding a body node if happen to be on a food
                body.Add(head);
                head.SetStatus(SpaceHolder.SpaceState.Body);
                head = head.GetRight();
                head.SetStatus(SpaceHolder.SpaceState.Head);
                gameSpace.SpawnFood();
            }
            else
            {
                // Here we collides with the snake itself, game over
                FindObjectOfType<Player>().GameEnd();
                Destroy(this);
            }
        }
        else if(dir == 'l')
        {
            tempState = head.GetLeft().GetStatus();
            if (tempState == SpaceHolder.SpaceState.Empty)
            {
                // reshape the body if nothing on the corresponding place
                body.Add(head);
                head.SetStatus(SpaceHolder.SpaceState.Body);
                head = head.GetLeft();
                head.SetStatus(SpaceHolder.SpaceState.Head);
                body[0].SetStatus(SpaceHolder.SpaceState.Empty);
                body.RemoveAt(0);
            }
            else if (tempState == SpaceHolder.SpaceState.Food)
            {
                // adding a body node if happen to be on a food
                body.Add(head);
                head.SetStatus(SpaceHolder.SpaceState.Body);
                head = head.GetLeft();
                head.SetStatus(SpaceHolder.SpaceState.Head);
                gameSpace.SpawnFood();
            }
            else
            {
                FindObjectOfType<Player>().GameEnd();
                Destroy(this);
            }
        }
        else if (dir == 'u')
        {
            tempState = head.GetUp().GetStatus();
            if (tempState == SpaceHolder.SpaceState.Empty)
            {
                // reshape the body if nothing on the corresponding place
                body.Add(head);
                head.SetStatus(SpaceHolder.SpaceState.Body);
                head = head.GetUp();
                head.SetStatus(SpaceHolder.SpaceState.Head);
                body[0].SetStatus(SpaceHolder.SpaceState.Empty);
                body.RemoveAt(0);
            }
            else if (tempState == SpaceHolder.SpaceState.Food)
            {
                // adding a body node if happen to be on a food
                body.Add(head);
                head.SetStatus(SpaceHolder.SpaceState.Body);
                head = head.GetUp();
                head.SetStatus(SpaceHolder.SpaceState.Head);
                gameSpace.SpawnFood();
            }
            else
            {
                FindObjectOfType<Player>().GameEnd();
                Destroy(this);
            }
        }
        else if (dir == 'd')
        {
            tempState = head.GetDown().GetStatus();
            if (tempState == SpaceHolder.SpaceState.Empty)
            {
                // reshape the body if nothing on the corresponding place
                body.Add(head);
                head.SetStatus(SpaceHolder.SpaceState.Body);
                head = head.GetDown();
                head.SetStatus(SpaceHolder.SpaceState.Head);
                body[0].SetStatus(SpaceHolder.SpaceState.Empty);
                body.RemoveAt(0);
            }
            else if (tempState == SpaceHolder.SpaceState.Food)
            {
                // adding a body node if happen to be on a food
                body.Add(head);
                head.SetStatus(SpaceHolder.SpaceState.Body);
                head = head.GetDown();
                head.SetStatus(SpaceHolder.SpaceState.Head);
                gameSpace.SpawnFood();
            }
            else
            {
                FindObjectOfType<Player>().GameEnd();
                Destroy(this);
            }
        }
    }
}
