﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSpace : MonoBehaviour {

    //SERIALIZED FIELD
    // game space size specification
    [SerializeField] int width = 20;
    [SerializeField] int height = 20;
    //place holder for space
    [SerializeField] GameObject space;

    //PRIVATE FIELDS
    private SpaceHolder[][] gameMap;

	// Use this for initialization
	void Start () {
        // Initialize the game map
        gameMap = new SpaceHolder[height][];

        // a temp space holder object
        GameObject temp;
        for (int i=0; i<height; i++)
        {
            gameMap[i] = new SpaceHolder[width];
            for (int j=0; j<width; j++)
            {
                temp = Instantiate(space, new Vector3(transform.position.x + j, transform.position.y + i, transform.position.z), transform.rotation);
                temp.transform.parent = transform;
                gameMap[i][j] = temp.GetComponent<SpaceHolder>();
            }
        }

        // Link all the spaces
        for(int i=0; i<height; i++)
        {
            for(int j=0; j<width; j++)
            {
                gameMap[i][j].SetUp(gameMap[(i + 1) % height][j]);  // set up space
                gameMap[i][j].SetDown(gameMap[(i - 1 + height) % height][j]);    // set down space
                gameMap[i][j].SetLeft(gameMap[i][(j - 1 + width) % width]); // set left space
                gameMap[i][j].SetRight(gameMap[i][(j + 1) % width]);    //set right space
            }
        }

        // Initialize a snake
        SpaceHolder snakeHead = gameMap[height / 2][width / 2];
        snakeHead.SetStatus(SpaceHolder.SpaceState.Head);
        SpaceHolder body_1 = snakeHead.GetDown();
        body_1.SetStatus(SpaceHolder.SpaceState.Body);
        SpaceHolder body_2 = snakeHead.GetDown().GetDown();
        body_2.SetStatus(SpaceHolder.SpaceState.Body);

        GetComponent<Snake>().SpawnSnake(snakeHead, body_1, body_2);
    }

    // Spawn a food location
    public void SpawnFood()
    {
        // find an empty space to generate the food
        while (!SpawnFoodAt((int)Random.Range(0, height), (int)Random.Range(0, width)));
    }

    private bool SpawnFoodAt(int x, int y)
    {
        // check if the specified space is empty
        if(gameMap[y][x].GetStatus() == SpaceHolder.SpaceState.Empty)
        {
            // make the sapce a food
            gameMap[y][x].SetStatus(SpaceHolder.SpaceState.Food);
            return true;
        }
        return false;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
